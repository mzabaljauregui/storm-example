package storm.example.event;


/**
 * Rep log off event. Is launched when some logs out from the callcenter.
 *
 */
public class RepLogOff extends AgentEvent {

    public RepLogOff(Integer id, Integer agentId, Long time, Integer callcenterId) {
        super(id, agentId, time, callcenterId);
    }
}

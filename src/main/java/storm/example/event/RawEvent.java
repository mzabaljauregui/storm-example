package storm.example.event;

/**
 * Raw Event abstract class. All the raw events will extend 
 * from this class in order to get the common properties such as id.
 *
 */
public abstract class RawEvent extends Event implements IRawEvent {
    
    private Integer id;
    
    /**
     * Main RawEvent constructor
     * @param id
     * @param time
     * @param callcenterId
     */
    public RawEvent(Integer id, Long time, Integer callcenterId) {
        super(time, callcenterId);
        this.id = id;
    }
    
    public Integer getId() {
        return id;
    }
}

package storm.example.event;


/**
 * Agent Event abstract class. All the events that has an agent related to it will 
 * extend from this class in order to get the common properties, such as the 'agentId'.
 *
 */
public abstract class AgentEvent extends RawEvent implements IAgentEvent {

    private Integer agentId;
    
    /**
     * Main constructor of an agent event
     * @param agentId
     * @param time
     */
    public AgentEvent(Integer id, Integer agentId, Long time, Integer callcenterId) {
        super(id, time, callcenterId);
        this.agentId = agentId;
    }
    
    public Integer getAgentId() {
        return agentId;
    }
}

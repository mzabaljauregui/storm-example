package storm.example.event;


/**
 * Rep log on event. Is launched when some logs in at the callcenter.
 *
 */
public class RepLogOn extends AgentEvent {

    public RepLogOn(Integer id, Integer agentId, Long time, Integer callcenterId) {
        super(id, agentId, time, callcenterId);
    }
}

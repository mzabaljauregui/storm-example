package storm.example.event;

/**
 * Common Agent Events interface
 *
 */
public interface IAgentEvent extends IRawEvent {

    /**
     * Gets the agent id that launched the event
     * @return agentId
     */
    public Integer getAgentId();
}

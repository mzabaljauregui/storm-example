package storm.example.event;


/**
 * Call start event. Is launched when some starts a phone call
 *
 */
public class CallStart extends RawEvent {
    
    private final Integer sessionId;
    
    public CallStart(Integer id, Integer sessionId, Long startTime, Integer callcenterId) {
        super(id, startTime, callcenterId);
        this.sessionId = sessionId;
    }

    public Integer getSessionId() {
        return sessionId;
    }
}

package storm.example.event;


/**
 * Main Event abstract class. All the events will extend from this class in order
 * to get the common properties, such as the 'id' and a 'timestamp' of occurrence.
 *
 */
public abstract class Event implements IEvent {

    private Long time;
    private Integer callcenterId;
    
    /**
     * Main constructor of an agent event
     * @param id
     * @param time
     */
    public Event(Long time, Integer callcenterId) {
        this.time = time;
        this.callcenterId = callcenterId;
    }

    public Long getTime() {
        return time;
    }
    
    public Integer getCallcenterId() {
        return callcenterId;
    }
}

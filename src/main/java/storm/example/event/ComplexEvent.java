package storm.example.event;

/**
 * Complex Event abstract class. All the complex events will extend 
 * from this class in order to get the common properties.
 *
 */
public abstract class ComplexEvent extends Event implements IComplexEvent {

    /**
     * Main ComplexEvent constructor
     * @param time
     * @param callcenterId
     */
    public ComplexEvent(Long time, Integer callcenterId) {
        super(time, callcenterId);
    }

}

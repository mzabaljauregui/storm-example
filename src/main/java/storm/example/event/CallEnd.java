package storm.example.event;


/**
 * Call end event. Is launched when some ends a phone call
 *
 */
public class CallEnd extends RawEvent {

    private final Integer sessionId;
    
    public CallEnd(Integer id, Integer sessionId, Long endTime, Integer callcenterId) {
        super(id, endTime, callcenterId);
        this.sessionId = sessionId;
    }
    
    public Integer getSessionId() {
        return sessionId;
    }
}

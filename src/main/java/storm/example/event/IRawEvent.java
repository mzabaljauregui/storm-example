package storm.example.event;

/**
 * Raw Event interface
 *
 */
public interface IRawEvent extends IEvent {

    /**
     * Get the unique id for the raw event occurrence.
     * @return id
     */
    public Integer getId();
}

package storm.example.event;

/**
 * Complex call event. Is launched when a call has started and ended.
 * Unifies both events to a unique one.
 *
 */
public class FinishedCall extends ComplexEvent {

    private Long sessionLength;
    private String view;

    /**
     * FinishedCall constructor.
     * @param sessionLength
     * @param end
     * @param callcenterId
     * @param view
     */
    public FinishedCall(Long sessionLength, Long end, Integer callcenterId, String view) {
        super(end, callcenterId);
        this.sessionLength = sessionLength;
        this.view = view;
    }

    public Long getSessionLength() {
        return sessionLength;
    }
    
    public String getView() {
        return view;
    }
}

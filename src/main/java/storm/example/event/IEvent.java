package storm.example.event;


/**
 * Common Events interface. For now only specifies the behavior of
 * having a timestamp added to every event occurrence and a unique id.
 *
 */
public interface IEvent {

    /**
     * Get the timestamp from the event occurrence. In the future
     * We should use String or another types instead of a hole Date 
     * object to improve performance.
     * @return time
     */
    public Long getTime();
    
    /**
     * Get the callcenter id where this event was originated.
     * @return callcenterId
     */
    public Integer getCallcenterId();
}

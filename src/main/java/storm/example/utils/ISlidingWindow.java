package storm.example.utils;

import java.util.Collection;

import storm.example.event.AgentEvent;
import storm.example.event.IEvent;

/**
 * TODO: Change this to param IEvent
 * @param <T>
 */
public interface ISlidingWindow<T extends IEvent> {

    public long getWindowLength();
    public Collection<T> getValid();
    public void insert(T event);

}

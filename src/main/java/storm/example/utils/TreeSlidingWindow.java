package storm.example.utils;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import storm.example.event.IEvent;

public class TreeSlidingWindow<T extends IEvent> implements ISlidingWindow<T> {

    private long length;
    private SortedMap<Key<T>, T> set;
    
    /**
     * 
     * @param length
     */
    public TreeSlidingWindow(long length) {
        checkArgument(length > 0);
        this.length = length;
        this.set = Collections.synchronizedSortedMap(new TreeMap<Key<T>, T>());
        
        // A thread that cleans up old events every ten seconds.
        startCleaner();
    }
    
    /**
     * Get the sliding window length which has been set
     * @return length
     */
    public long getWindowLength() {
        return length;
    }

    /**
     * Get all elements IEvent that are valid for the time window
     * @return events
     */
    public Collection<T> getValid() {
        long now = System.currentTimeMillis();
        long from = now - length;

        return set.tailMap(new Key<T>(from, null)).values();
    }

    /**
     * Insert a new IEvent into the buffer
     */
    public void insert(T event) {
        set.put(new Key<T>(event.getTime(), event), event);
    }
    
    /**
     * Returns the value to which the specified key is mapped,
     * or {@code null} if this map contains no mapping for the key.
     */
//    TODO: check how to implement this
//    public IEvent get(Integer id, Long time) {
//        return set.get(new Key(id, time));
//    }

    /**
     * Get the events that doesn't match the window length set.
     * @return events
     */
    private SortedMap<Key<T>, T> getInvalid() {
        long now = new Date().getTime();
        long to = now - length;

        return set.headMap(new Key<T>(to, null));
    }
    
    /**
     * Clean the events from the buffer that doesn't match the 
     * window length set. 
     */
    private void cleanOld() {
        // Need to make a copy of the invalid keys to avoid a concurrent modification exception.
        for (Key<T> k : new LinkedList<Key<T>>(getInvalid().keySet())) {
            System.out.println("Evicting old event: " + k);
            set.remove(k);
        }
        
        System.out.println("Size: " + set.size());
    }
    
    /**
     * Cleaner method invoked by the TreeSlidingWindow thread in order
     * to work apart from the main TreeSlidingWindow thread. Will have his own
     * event occurrence which is attached at a timer (as a chrone job).
     */
    private void startCleaner() {
        final TreeSlidingWindow<T> me = this;

        new Timer(true).scheduleAtFixedRate(new TimerTask() {
            public void run() { System.out.println("Cleaning up."); me.cleanOld(); }
        }, 0, 1 * 1000);
    }
    
    /**
     * The key under which to store the events in the TreeMap
     * It compares using the timestamp, breaking ties with an id that should be
     * guaranteed to be unique.
     * @author bruno
     *
     */
    private static class Key<T extends IEvent> implements Comparable<Key<T>> {

        Long time;
        T value;

        Key(Long time, T value) {
            this.time = time;
            this.value = value;
        }

        public int compareTo(Key<T> o) {
            if (this.time < o.time) return -1;
            else if (this.time > o.time) return +1;
            else if (value != null && value.equals(o.value)) return 0;
            else return +1;
        }

        @Override
        public String toString() {
            return "Key [timestamp=" + time + ", value=" + value + "]";
        }
    }    
}

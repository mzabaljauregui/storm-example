package storm.example.utils;

/**
 * Possible event types available from the Row Queue.
 * There are complex events and simple ones.
 *
 */
public enum EventType {

    FINISHEDCALL,       // finishedcall: complex CEP event built when a phone call has started and ended
    CALLSTART, // callstart: event emitted when a phone call has started 
    CALLEND,   // callend: event emitted when a phone call has ended
    REPLOGOFF,      // replogoff: event emitted when a rep has logged in the callcenter
    REPLOGON      // replogon: event emitted when a rep has logged out the callcenter
}

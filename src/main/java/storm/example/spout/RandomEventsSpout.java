/**
 * 
 */
package storm.example.spout;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import storm.example.domain.Agent;
import storm.example.domain.Phone;
import storm.example.event.CallEnd;
import storm.example.event.CallStart;
import storm.example.event.IEvent;
import storm.example.event.RepLogOff;
import storm.example.event.RepLogOn;
import storm.example.utils.EventType;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;

/**
 * This class will launch randomly agent events into the topology
 * implemented.
 */
public class RandomEventsSpout extends BaseRichSpout  {

    // Properties required by Spouts
    SpoutOutputCollector _collector;

    // Own properties
    private Random _rand;  
    private List<Agent> agents = new ArrayList<Agent>();
    private List<Phone> phones = new ArrayList<Phone>();
    private Integer uniqueId = 0;

    /**
     * Method implemented from ISpout.
     */
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        
        this.initAgents();
        this.initPhoneCalls();

        _collector = collector;
        _rand = new Random();

    }

    /**
     * Method implemented from ISpout.
     * Emits the next tuple. Could be a 'replogon/replogoff' from one of the agents
     * initialized. It also emits phone call events such as 'callstart/callend'.
     */
    public void nextTuple() {

        Utils.sleep(100);

        // Emit randomly an agent login/logoff event
        randomAgentLogEvent();
        
        // Emit randomly a phone call start/end event
        randomPhoneCallEvent();
    }

    /**
     * Method implemented from IComponent.
     * Topic name of the output stream
     */
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("eventType", "event"));
    }

    /**
     * Emit a random agent logged event
     */
    private void randomAgentLogEvent() {

        EventType type;
        IEvent event;

        // Get randomly an agent and change his logged state
        Agent agent = this.agents.get(_rand.nextInt(this.agents.size()));
        agent.loggedStatusChange();

        if(agent.isLogged()) {
            type = EventType.REPLOGON;
            event = new RepLogOn(nextId(), agent.getId(), new Date().getTime(), agent.getCallcenterId());
        } else {
            type = EventType.REPLOGOFF;
            event = new RepLogOff(nextId(), agent.getId(), new Date().getTime(), agent.getCallcenterId());
        }

        // Emit the corresponding logged event
        _collector.emit(new Values(type, event));
    }
    
    /**
     * Emit a random phone call event
     */
    private void randomPhoneCallEvent() {

        EventType type;
        IEvent event;
        
        Phone phoneCall = this.phones.get(_rand.nextInt(this.phones.size()));
        phoneCall.changeProgressStatus();
        
        if(phoneCall.isInProgress()) {
            type = EventType.CALLSTART;
            event = new CallStart(nextId(), phoneCall.getId(), new Date().getTime(), phoneCall.getCallcenterId());
        } else {
            type = EventType.CALLEND;
            event = new CallEnd(nextId(), phoneCall.getId(), new Date().getTime(), phoneCall.getCallcenterId());
        }

        // Emit the corresponding phone call event
        _collector.emit(new Values(type, event));
    }
    
    /**
     * Initialization of dummy agents
     */
    private void initAgents(){

        agents.add(new Agent(1, "name1", false, 1));
        agents.add(new Agent(2, "name2", false, 1));
        agents.add(new Agent(3, "name3", false, 1));
        agents.add(new Agent(4, "name4", false, 1));
        agents.add(new Agent(5, "name5", false, 1));
        agents.add(new Agent(6, "name6", false, 2));
        agents.add(new Agent(7, "name7", false, 2));
        agents.add(new Agent(8, "name8", false, 2));
        agents.add(new Agent(9, "name9", false, 2));
        agents.add(new Agent(10, "name10", false, 2));
    }
    
    /**
     * Initialization of dummy phones 
     */
    private void initPhoneCalls(){

        this.phones.add(new Phone(1, 1));
        this.phones.add(new Phone(2, 1));
        this.phones.add(new Phone(3, 1));
        this.phones.add(new Phone(4, 1));
        this.phones.add(new Phone(5, 1));
        this.phones.add(new Phone(6, 2));
        this.phones.add(new Phone(7, 2));
        this.phones.add(new Phone(8, 2));
        this.phones.add(new Phone(9, 2));
        this.phones.add(new Phone(10, 2));
    }
    
    /**
     * Get and increment unique id
     * @return id
     */
    private Integer nextId() {
        
        return uniqueId++;
    }
}

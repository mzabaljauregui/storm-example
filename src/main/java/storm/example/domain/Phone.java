package storm.example.domain;

/**
 * Phone class
 * This class represents a telephone that will originate a call
 * into the system.
 */
public class Phone {
    
    // Properties
    private int id;
    private boolean inProgress = false;
    private int callcenterId;
    
    /**
     * Phone constructor.
     * @param id
     * @param callcenterId
     */
    public Phone(int id, int callcenterId) {
        this.id = id;
        this.callcenterId = callcenterId;
    }
    
    public void changeProgressStatus() {
        this.inProgress = !this.inProgress;
    }
    
    // Getters
    public int getId() {
        return id;
    }
    public boolean isInProgress() {
        return inProgress;
    }
    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }
    public int getCallcenterId() {
        return callcenterId;
    }
}

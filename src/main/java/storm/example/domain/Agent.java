package storm.example.domain;


/**
 * Agent class
 */
public class Agent {

    //Properties
    private int id;
    private String name;
    private boolean logged;
    private int callcenterId;

    /**
     * Constructor
     * @param id
     * @param name
     * @param logged
     * @param callcenterId
     */
    public Agent(int id, String name, boolean logged, int callcenterId) {
        this.id = id;
        this.name = name;
        this.logged = logged;
        this.callcenterId = callcenterId;
    }

    public void loggedStatusChange() {
        this.logged = !this.logged;
    }
    
    //Getters
    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public boolean isLogged() {
        return logged;
    }
    public int getCallcenterId() {
        return callcenterId;
    }
}

package storm.example;

import storm.example.event.FinishedCall;
import storm.example.event.IEvent;
import storm.example.utils.TreeSlidingWindow;

public class TestSlidingWindow {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        
        Long endTime = System.currentTimeMillis();
        Long sessionLength = endTime + 100;
        Integer callcenterId1 = 1;
        Integer callcenterId2 = 2;
        
        FinishedCall e1 = new FinishedCall(sessionLength, endTime, callcenterId1, "CCID1");
        FinishedCall e2 = new FinishedCall(sessionLength, endTime, callcenterId1, "CCID1");
        FinishedCall e3 = new FinishedCall(sessionLength, endTime, callcenterId2, "CCID2");
        FinishedCall e4 = new FinishedCall(sessionLength, endTime, callcenterId2, "CCID2");

        TreeSlidingWindow<FinishedCall> calls = new TreeSlidingWindow<FinishedCall>(1000);
        calls.insert(e1);
        calls.insert(e2);
        calls.insert(e3);
        calls.insert(e4);

//        TODO: Implement and test a get method for the TreeSlidingWindow
//        IEvent call = calls.get(1, now);
//        System.out.println("call: " + call);

        for (IEvent e : calls.getValid()) {
            System.out.println("Validas: " + e);
        }

        Thread.sleep(2000);

        // No more valid events after two seconds for a one second time window.
        assert calls.getValid().size() == 0;
        
        for (IEvent e : calls.getValid()) {
            System.out.println("Validas: " + e);
        }
    }

}

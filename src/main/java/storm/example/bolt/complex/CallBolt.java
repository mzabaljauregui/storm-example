/**
 * 
 */
package storm.example.bolt.complex;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import storm.example.event.FinishedCall;
import storm.example.event.CallEnd;
import storm.example.event.CallStart;
import storm.example.event.Event;
import storm.example.event.IEvent;
import storm.example.utils.EventType;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/**
 * Bolt that processes the call start/end events. 
 * This class produces and forwards a complex FinishedCall event using
 * a temporary map to follow the started calls.
 */
public class CallBolt extends BaseBasicBolt {

    // TODO: See how to implement an expiration policy for orphaned calls 
    private Map<Integer, Call> calls; // Map<sessionId, startTime>
    
    @SuppressWarnings("rawtypes")
    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        
        this.calls = new ConcurrentHashMap<Integer, Call>();
        super.prepare(stormConf, context);
    }
    
    /**
     * Execute method implementation from the IBasicBolt
     */
    public void execute(Tuple input, BasicOutputCollector collector) {

        // Get the session event type
        EventType type = (EventType)input.getValue(0);
        
        // Get the session event
        IEvent event = (Event)input.getValue(1);
        
        // Get the view
        String view = (String)input.getValue(2);
        
        // Session started
        if(EventType.CALLSTART.equals(type)) {
            
            // Create a new call at the temporary map
            updateCallsWithStart((CallStart)event);
        
        // Session end
        } else if(EventType.CALLEND.equals(type)) {
            
            CallEnd callEnd = (CallEnd)event;
            
            // Update the temporary map
            updateCallsWithEnd(callEnd);
            
            // Create the finishedCall complex event
            FinishedCall finishedCall = createFinishedCall(callEnd, view);

            // If the start and end events where both received
            if(finishedCall != null) {
             
                // Emit the call event forward
                collector.emit(new Values(EventType.FINISHEDCALL, finishedCall, view));

                // Remove the emitted call from the temporary started calls map
                removeCall(callEnd.getSessionId());
            }    
        }
    }

    /**
     * Declare output fields
     */
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("type", "callEvent", "view"));
    }
    
    /**
     * Update the temporary Call instance at the calls map with the startTime.
     * If not exists, a new temporary Call instance will be created and
     * set at that map.
     * @param callStart
     */
    private void updateCallsWithStart(CallStart callStart) {
        
        Call call = this.calls.get(callStart.getSessionId());
        if(call != null) {
            call.setStartTime(callStart.getTime());
        } else {
            call = new Call(callStart.getTime(), null);
            this.calls.put(callStart.getSessionId(), call);
        }
    }
    
    /**
     * Update the temporary call instance at the calls map with the endTime.
     * If not exists, a new temporary Call instance will be created and
     * set at that map. This happens when an endCall is received 
     * before the startCall event for the same session.
     * @param callEnd
     */
    private void updateCallsWithEnd(CallEnd callEnd) {

        Call call = this.calls.get(callEnd.getSessionId());
        if(call != null) {
            call.setEndTime(callEnd.getTime());
        } else {
            call = new Call(null, callEnd.getTime());
            this.calls.put(callEnd.getSessionId(), call);
        }
    }
    
    /**
     * Call inner class. Will contain the startTime and endTime
     * as properties
     *
     */
    private class Call {
        
        private Long startTime;
        private Long endTime;
        
        public Call(Long startTime, Long endTime) {
            this.startTime = startTime;
            this.endTime = endTime;
        }

        public Long getStartTime() {
            return startTime;
        }
        public void setStartTime(Long startTime) {
            this.startTime = startTime;
        }
        public Long getEndTime() {
            return endTime;
        }
        public void setEndTime(Long endTime) {
            this.endTime = endTime;
        }
    }
    
    /**
     * FinishedCall method. 
     * Responsible of getting the startTime from the startedCalls 
     * map and create the finishedCall event.
     * @param callEnd
     * @param view
     * @return finishedCall
     */
    private FinishedCall createFinishedCall(CallEnd callEnd, String view) {
        
        FinishedCall finishedCall = null;
        
        // Check if both start and end time where set for that session call and then create
        // the finishedCall.
        Call call = this.calls.get(callEnd.getSessionId());
        if (call.getStartTime() != null && call.getEndTime() != null) {
            finishedCall = new FinishedCall(call.getEndTime() - call.getStartTime(), call.getEndTime(), 
                    callEnd.getCallcenterId(), view);
        }
        return finishedCall;
    }
    
    /**
     * Remove a call from the temporary startedCalls map
     * @param key
     */
    private void removeCall(Integer key) {
        this.calls.remove(key);
    }
}
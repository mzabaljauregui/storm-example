/**
 * 
 */
package storm.example.bolt.classifier;

import storm.example.utils.EventType;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

/**
 * Bolt that classifies the rep logon/logoff events.
 */
public class LogEventBolt extends BaseBasicBolt {

    /**
     * Execute method implementation from the IBasicBolt
     */
    public void execute(Tuple input, BasicOutputCollector collector) {

        // Getting the event type
        EventType type = (EventType)input.getValue(0);

        // Check if the event type belongs to this Bolt
        if(EventType.REPLOGON.equals(type) || EventType.REPLOGOFF.equals(type)) {

            // Forward the tuple for processing
            collector.emit(input.getValues());
        }
    }

    /**
     * Declare output fields
     */
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("type", "logEvent", "view"));
    }
}
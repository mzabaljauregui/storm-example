/**
 * 
 */
package storm.example.bolt.classifier;

import storm.example.event.Event;
import storm.example.event.IEvent;
import storm.example.utils.EventType;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/**
 * Bolt that processes the call events
 */
public class ViewClassifierBolt extends BaseBasicBolt {

    /**
     * Execute method implementation from the IBasicBolt
     */
    public void execute(Tuple input, BasicOutputCollector collector) {

        // Get the event type
        EventType type = (EventType)input.getValue(0);
        
        // Get the session event
        IEvent event = (Event)input.getValue(1);

        // Relate with his view
        // TODO: For now, there will be only a callcenterId-view available
        // so we will temporary create the view instance like this.
        String view = "CCID" + event.getCallcenterId().toString();
        
        // Forward the event with the view
        collector.emit(new Values(type, event, view));
    }

    /**
     * Declare output fields
     */
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("eventType", "event", "view"));
    }
}
/**
 * 
 */
package storm.example.bolt.metric;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import storm.example.utils.EventType;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

/**
 * Bolt that performs the agents online metric.
 */
public class AgentsLoggedBolt extends BaseBasicBolt {

    private Map<String, Integer> metrics; // Map<View, Metric>
    private JedisPool pool;
    private String metricName;
    
    /**
     * Constructor
     */
//    public AgentsLoggedBolt(JedisPool pool) {
//        this.pool = pool;
//    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public void prepare(Map stormConf, TopologyContext context) {

        this.metrics = new ConcurrentHashMap<String, Integer>();
        this.metricName = "agentsLogged";
        this.pool = new JedisPool(new JedisPoolConfig(), "localhost");
        super.prepare(stormConf, context);
    }

    /**
     * Execute method implementation from the IBasicBolt
     */
    public void execute(Tuple input, BasicOutputCollector collector) {

        // Get the event type
        EventType type = (EventType)input.getValue(0);

        // Get the view
        String view = (String)input.getValue(2);

        //TODO: still have to check login events consistency (in here?)

        // Processes the data
        updateMetrics(type, view);
        
        // Output
        printMetric();
    }

    /**
     * Clean up after end
     */
    @Override
    public void cleanup() {
        this.pool.destroy();
        super.cleanup();
    }
    
    /**
     * Declare output fields implementation
     */    
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("results"));
    }
    
    /**
     * Update agents logged counter's map
     * @param type
     * @param view
     */
    public void updateMetrics(EventType type, String view) {
        
        // Create new entry if not exists
        if (!this.metrics.containsKey(view)) {
            this.metrics.put(view, 0);
        }
        
        // Update value
        Integer value = this.metrics.get(view);
        if(EventType.REPLOGON.equals(type)) {
            this.metrics.put(view, value + 1);
        } else {
            this.metrics.put(view, value - 1);
        }
    }
    
    /**
     * Print metrics map
     */
    public void printMetric() {
        
        // Get Jedis instance
        Jedis jedis = pool.getResource();

        for (Map.Entry<String, Integer> entry : this.metrics.entrySet()) {
            
            String view = entry.getKey();
            String key = view + "." + this.metricName;
            Integer agentsLogged = entry.getValue();
            
            // Console log
            System.out.println("[Agents logged metric] " + key + ": " + agentsLogged);
            
            // Redis persistence
            jedis.set(key, agentsLogged.toString());
        }
        
        // Return Jedis pool instance
        pool.returnResource(jedis);
    }
}
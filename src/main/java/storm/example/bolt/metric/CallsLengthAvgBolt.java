/**
 * 
 */
package storm.example.bolt.metric;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import storm.example.event.FinishedCall;
import storm.example.utils.EventType;
import storm.example.utils.TreeSlidingWindow;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

/**
 * Bolt that performs the average call length metric.
 */
public class CallsLengthAvgBolt extends BaseBasicBolt {

    private Map<String, Metric> metrics; // Map<View, Metric>
    private JedisPool pool;
    private String metricName;
    
    /**
     * Constructor
     */    
//    public CallsLengthAvgBolt(JedisPool pool) {
//        this.pool = pool;
//    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public void prepare(Map stormConf, TopologyContext context) {
        
        this.metrics = new ConcurrentHashMap<String, Metric>();
        this.metricName = "callsLengthAvg";
        this.pool = new JedisPool(new JedisPoolConfig(), "localhost");
        super.prepare(stormConf, context);
    }
    
    /**
     * Execute method implementation from the IBasicBolt
     */
    public void execute(Tuple input, BasicOutputCollector collector) {

        // Get the event type
        EventType type = (EventType)input.getValue(0);
        
        // Get the call event
        FinishedCall event = (FinishedCall)input.getValue(1);

        // Get the view
        String view = (String)input.getValue(2);
        
        // Call received
        if(EventType.FINISHEDCALL.equals(type)) {
            
            // Create a new call instance at the treemap buffer
            updateMetrics(view, event);
            
            // Processing valid calls in buffer
            printMetric();
        }
    }

    /**
     * Clean up after end
     */
    @Override
    public void cleanup() {
        this.pool.destroy();
        super.cleanup();
    }

    /**
     * Declare output fields implementation
     */
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("type", "loginEvent"));
    }

    /**
     * Metric inner class. 
     * This class has the necessary properties to track and process
     * the call metric for each view.
     */
    private class Metric {
        
        private Long totalCallsLength;
        private Integer totalCalls;
        private TreeSlidingWindow<FinishedCall> calls;
        
        public Metric() {
            
            this.totalCallsLength = 0L;
            this.totalCalls = 0;
            this.calls = new TreeSlidingWindow<FinishedCall>(10000);
        }
        
        public void insertCall(FinishedCall event) {
            this.calls.insert(event);
        }
        
        public Collection<FinishedCall> getValidCalls() {
            return calls.getValid();
        }
        
        public void updateCalls(Long sessionLength) {
            this.totalCallsLength += sessionLength;
            this.totalCalls++;
        }
        
        public Long getTotalCallsLength() {
            return this.totalCallsLength;
        }
        
        public void setTotalCallsLength(Long totalCallsLength) {
            this.totalCallsLength = totalCallsLength;
        }
        
        public Integer getTotalCalls() {
            return this.totalCalls;
        }
        
        public void setTotalCalls(Integer totalCalls) {
            this.totalCalls = totalCalls;
        }
    }

    /**
     * Update the callMetrics map inserting the new call at the 
     * corresponding metric.
     * @param view
     * @param event
     */
    private void updateMetrics(String view, FinishedCall event) {
        
        // Create new entry if not exists
        if (!this.metrics.containsKey(view)) {
            this.metrics.put(view, new Metric());
        }
        
        // Update metric with a new call
        Metric metric = this.metrics.get(view);
        metric.insertCall(event);
    }

    /**
     * Print metrics map
     */
    public void printMetric() {
        
        // Get Jedis instance
        Jedis jedis = pool.getResource();

        // For each view -> process metric
        for (Map.Entry<String, Metric> entry : this.metrics.entrySet()) {
        
            String view = entry.getKey();
            Metric metric = entry.getValue();
            String key = view + "." + this.metricName;
            
            // Get the valid finished calls from the tree sliding window
            for (FinishedCall e : metric.getValidCalls()) {
                metric.updateCalls(e.getSessionLength());
            }
            
            // Average
            Double average = metric.getTotalCallsLength().doubleValue() / metric.getTotalCalls().doubleValue(); 

            // Console log
            System.out.println("[Calls average metric] " + view + " Total calls length:" + metric.getTotalCallsLength());
            System.out.println("[Calls average metric] " + view + " Total calls       :" + metric.getTotalCalls());
            System.out.println("[Calls average metric] " + view + " Average           :" + average);

            // Redis persistence
            jedis.set(key, average.toString());
            
            // Clean counter properties
            clean(metric);
        }
        
        // Return Jedis pool instance
        pool.returnResource(jedis);
    }

    /**
     * Clean all the properties involved at an specific metric
     * for processing
     */
    private void clean(Metric metric) {
        metric.setTotalCalls(0);
        metric.setTotalCallsLength(0L);
    }
}
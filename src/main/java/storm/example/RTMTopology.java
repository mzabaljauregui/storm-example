/**
 * 
 */
package storm.example;

import storm.example.bolt.classifier.CallEventBolt;
import storm.example.bolt.classifier.LogEventBolt;
import storm.example.bolt.classifier.ViewClassifierBolt;
import storm.example.bolt.complex.CallBolt;
import storm.example.bolt.metric.AgentsLoggedBolt;
import storm.example.bolt.metric.CallsLengthAvgBolt;
import storm.example.spout.RandomEventsSpout;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;

/**
 * @author mzabaljauregui
 *
 */
public class RTMTopology {

    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {

        TopologyBuilder builder = new TopologyBuilder();
        
        // Spout
        builder.setSpout("eventSpout", new RandomEventsSpout(), 1);

        // Bolt view classifier layer
        builder.setBolt("viewClassifierBolt", new ViewClassifierBolt(), 1).shuffleGrouping("eventSpout");
        builder.setBolt("logEventBolt", new LogEventBolt(), 1).fieldsGrouping("viewClassifierBolt", new Fields("view"));
        builder.setBolt("callEventBolt", new CallEventBolt(), 1).fieldsGrouping("viewClassifierBolt", new Fields("view"));

        // Bolt complex processing layer 
        builder.setBolt("callBolt", new CallBolt(), 1).shuffleGrouping("callEventBolt");
        
        // Bolt metric layer
        builder.setBolt("agentsLoggedBolt", new AgentsLoggedBolt(), 1)
            .fieldsGrouping("logEventBolt", new Fields("view"));
        builder.setBolt("callsLengthAvgBolt", new CallsLengthAvgBolt(), 1)
            .fieldsGrouping("callBolt", new Fields("view"));
        
        // Topology configuration
        Config conf = new Config();
        conf.setDebug(true);
        conf.setMaxTaskParallelism(3);

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("event-count", conf, builder.createTopology());

        Thread.sleep(1000000);

//      cluster.shutdown();
    }

}